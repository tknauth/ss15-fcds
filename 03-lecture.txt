# Lecture 3 - Linearizability - 2015-04-28

Concurrent object: how to describe? implement? tell if implementation
is correct, i.e., adheres to description.

public class Queue {
    int head = 0, tail = 0;
    Object[QSIZE] items;

    public synchronized void enq(Object x) {
        while (tail - head == QSIZE) this.wait();

        items[tail % QSIZE] = x;
        tail++;
        this.notifyAll();
    }

    public synchronized Object deq() {
        while (tail - head == 0) this.wait();

        Object x = items[head % QSIZE];
        head++;
        this.notifyAll();
        return x;
    }
}

Easy to reason about correctness, because all operations are mutually
exclusive, i.e., only one thread ever executes enq() or deq().

## Lock-free 2-Thread queue

public class LockFreeQueue {
    volatile int head = 0, tail = 0;

    Object[QSIZE] items;

    public void enq(Object x) {
        while (tail - head == QSIZE) {};
        items[tail % QSIZE] = x;
        tail++;
    }
    public Object deq() {
        while (tail == head) {};
        Object x = items[head % QSIZE];
        head++;
        return item;
    }
}

How to define "correctness" when modifications are not exclusive?

Object has fields (its state). Fields can be read and written using
methods. Sequential specification consists of pre-/post conditions.
Pre-/Postcondition for deq(): queue is not empty (pre), returns first queue
item (post), removes first item in queue (post).

Interactions among methods captured by side-effects on object state.
State meaningful between method calls. Documentation size linear in
number of methods. Each method described in isolation. Can add new
methods without changing description of old methods. What about
concurrent specifications?

Important for concurrent executions is that method calls take time
(non-zero execution time). Hence, concurrent method calls can overlap.
Sequential specification where we describe the state between method
calls with pre-/post conditions does not work.

Must characterize all possible interactions with concurrent calls.
What if 2 enq() overlap? 2 deq()? enq() and deq()? Everything can
potentially interact with everything else.

With mutual exclusion, each operation basically follows the pattern:
lock(), operation(), unlock(), e.g., q.deq() is lock(), deq(),
unlock().

Linearizability: each method should "take effect" instantaneously
between invocation and response events. Object is correct if this
"sequential" behavior is correct. Any such concurrent object is
linearizable. Formally, a linearizable object is an object all of
whose possible executions are linearizable.

Schedules can be linearizable in mutliple orders.

Multiple linearization points may exist within a method. May also
depend on the execution of concurrent methods. Must look at executions
and not only the code.

Split method calls into two events. Invocation (method name plus
arguments) and response (return value, exception, nothing).

Invocation notation: A q.enq(x)
Response notation: A q: void

    H
A q.enq(3)            
A q:void
B p.enq(4)
B p:void
B q.deq()
B q:3

Only interested in object q (called a projection)

    H|q
A q.enq(3)            
A q:void


B q.deq()
B q:3

Pending operations have not received a response yet, e.g., q.enq(5) in
the following example

A q.enq(3)
A q:void
A q.enq(5)
B p.enq(4)
B p:void
B q.deq()
B q:3

Complete histories leave out pending operations. The above history
becomes

A q.enq(3)
A q:void

B p.enq(4)
B p:void
B q.deq()
B q:3

Sequential histories. Method calls of different threads do not
interleave

A q.enq(3)
A q:void
B p.enq(4)
B p:void
B q.deq()
B q:3
A q:enq(5) // final pending invocation is OK

Well-formed histories: per-thread projections are sequential

A q.enq(3)
B p.enq(4)
B p:void
B q.deq()
A q:void
B q:3

H|B

B p.enq(4)
B p:void
B q.deq()
B q:3

H|A

A q.enq(3)
A q:void

Equivalent histories

     H
A q.enq(3)
B p.enq(4)
B p:void
B q.deq()
A q:void
B q:3

    G
A q.enq(3)
A q:void
B p.enq(4)
B p:void
B q.deq()
B q:3

Threads see the same thing in both histories if:
H|A = G|A and H|B = G|B

A sequential (multi object) history H is legal if for every object x
H|x is in the sequential specification for x.

A method call precedes another if response event precedes invocation
event.

A q.enq(3)
B p.enq(4)
B p:void
A q:void
B q.deq()
B q:3

q.enq(3) precedes q.deq():3

Non-precedence: some method calls overlap each other.

A q.enq(3)
B p.enq(4)
B p:void
B q.deq()
A q:void
B q:3

q.enq(3) and q.deq():3 overlap each other.

History H is linearizable if it can be extended to G by
    - appending zero of more responses to pending invocations
    - discarding other pending invocations
such that G is equivalent to a legal sequantial history S where the
precedes relation of G includes the precedes relation of S

What is ->G \subset ->S

->G = {a->c, b->c}            // partial (concurrent) order
->S = {a->b, a->c, b->c}  // total (sequential) order

Concurrent execution is "embedded" in sequential order.

Example: an incomplete history H

A q.enq(3)
B q.enq(4)
B q:void
B q.deq()
B q:4
B q.enq(6)

Add A q:void to complete history.

Equivalent sequential history.

B q.enq(4)
B q:void
A q.enq(3)
A q:voud
B q.deq()
B q:4

History H is linearizable if and only if for every object x H|x is
linearizable. We care about objects only. Why does locality matter?
Modularity. Can prove linearizability of objects in isolation. Can
compose independently-implemented objects.

Linearization of synchronized methods is after the loop where we wait
to perform the action, e.g., after this.wait() in the earlier example
of a concurrent queue. For lock-free implementation the linearization
point is the tail++ or head++ in the enq() and deq() methods
respectively.

How to find linearization points? Identify one atomic step where
method "happens", e.g., a critical section or specific machine
instruction. Not always applicable. Might need to define serveral
different steps for a given method.

Alternative to linearizability: sequential consistency. History H is
sequentially consistent if it can be extended to G by
    - appending zero or more responses to pending invocations

Sequential consistency is not a local property. Two objects which are
individually sequentially consistent, may not be seq. consistent if
combined. This is major difference compared to linearizability.

Seq. consistency used, for example, to describe shared memory
semantics. However, seq. consistency considered too expensive for
modern multi-core systems, i.e., the guarantees it provides are
considered too strong.

Opinion 1: totally wrong behavior. Breaks Bakery and Peterson
algorithm. Write my variable, read your variable.

Memory hierarchy of a modern multi-core system. Processors to not read
and write directly to memory. Memory accesses are very slow compared
to processors speeds. Processors writes into cache instead. There may
also be a write buffer, i.e., writes first go into the buffer.

Must explicitly tell the CPU that certain variables should have
stronger semantics, e.g., by using volatile keyword in Java. Explicit
synchronization via memory barrier instruction. Memory barrier flushes
unwritten caches and brings caches up to date. Compilers often do this
for you, e.g., when entering and leaving critical section. Also
disables reordering and other optimizations.

Real-world HW memory is weaker than sequential consistency, but you
can force sequential consistency (at a price). Linearizability more
appropriate for high-level software.

Critical sections are an easy way to achieve linearizability. Takes
sequential object and makes each method a critical section. For
example, synchronized methods in Java. It's blocking though and limits
concurrency.
