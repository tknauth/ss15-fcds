import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.math.BigInteger;


public class Main {

    static final long LIMIT = 1000000000L;

    private interface Counter {
        public long getAndIncrement();
    }
    
    private static class SynchronizedCounter implements Counter {
        long counter = 0;
        public long getAndIncrement() {
            long local;
            synchronized (this) {
                local = counter++;
            }
            return local;
        }
    }

    private static class AtomicCounter implements Counter {
        AtomicLong anAtomicLong = new AtomicLong();
        public long getAndIncrement() {
            return anAtomicLong.getAndAdd(1);
        }
    }
    
    private static class Compute implements Runnable {
        static Counter c;
        
        public long primeCounter = 0;
        public void run() {
            long d = c.getAndIncrement();
            do {
                // Test primality
                if (Main.mrPrimalityProven(d)) {
                    primeCounter++;
                }
                d = c.getAndIncrement();
            } while (d < Main.LIMIT);
        }
    }

    public static boolean mrPrimalityProven(long n) {
        if (n > 341550071728321L) {
            throw new UnsupportedOperationException("argument too big");
        }
        int[] trivial = {2, 3, 5, 7, 11, 13, 17};
        for (int x : trivial) {
            if (n == x) return true;
        }
        if (n == 1 || (n % 2) == 0) return false;
        long d = n - 1;
        while ((d & 1) == 0) { d >>= 1; }
    
        for (int a : new int[] {2, 3, 5, 7, 11, 13, 17}) {
            long t = d;
            long y = BigInteger.valueOf(a).modPow(BigInteger.valueOf(t), BigInteger.valueOf(n)).longValue();
            while (t != n-1 && y != 1 && y != n-1) {
                y = (y * y) % n;
                t <<= 1;
            }
            if (y != n-1 && (t & 1) == 0) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {

        if (args[1].equals("atomic")) { Compute.c = new AtomicCounter(); }
        else if (args[1].equals("synchronized")) { Compute.c = new SynchronizedCounter(); }
        else { throw new RuntimeException(); }
        
        int threads = Integer.parseInt(args[0]);
        List<Thread> ts = new ArrayList<Thread>();
        List<Compute> computes = new ArrayList<Compute>();
        for (int i = 0; i < threads; ++i) {
            Compute c = new Compute();
            Thread t = new Thread(c);
            t.start();
            ts.add(t);
            computes.add(c);
        }

        for (Thread t : ts) {
            try {
                t.join();
                
            } catch (InterruptedException e) { throw new RuntimeException(); }
        }

        long totalPrimes = 0;
        for (Compute c : computes) {
            totalPrimes += c.primeCounter;
        }

        System.out.println("found "+totalPrimes+" primes");
        // https://www.wolframalpha.com/input/?i=how+many+primes+are+there+below+10%5E9
        if (totalPrimes != 50847534) throw new RuntimeException();

    }
}
