public class BakeryLock implements Lock {

    private volatile boolean flag[];
    private volatile int[] label;

    public BakeryLock(int n) {
        flag = new boolean[n];
        label = new int[n];
        for (int i = 0; i < n; ++i) {
            flag[i] = false;
            label[i] = 0;
        }
    }

    private int max_label() {
        int max = Integer.MIN_VALUE;
        for (int i : label) {
            if (i > max) {
                max = i;
            }
        }
        return max;
    }

    private boolean someone_interested(int thread_id) {
        for (int k = 0; k < flag.length; ++k) {
            if (flag[k] &&
                ((label[thread_id] > label[k]) ||
                 (label[thread_id] == label[k] && thread_id > k))) {

                return true;
            }
        }
        return false;
    }
    
    public void lock(int thread_id) {
        flag[thread_id] = true;
        label[thread_id] = max_label() + 1;

        while (someone_interested(thread_id)) {
            // If we don't yield here, execution time can terrible: on
            // a 2-core machine with 3 threads incrementing a shared
            // counter up to 10^5 took 399 seconds (> 6 minutes).
            Thread.yield();
        }
    }
    
    public void unlock (int thread_id) {
        flag[thread_id] = false;
    }
}
