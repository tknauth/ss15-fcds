public class PetersonLock implements Lock {

    private volatile boolean flag[];
    private volatile int victim;

    public PetersonLock() {
        flag = new boolean[2];
        flag[0] = flag[1] = false;
    }
    
    public void lock(int thread_id) {
        flag[thread_id] = true;
        victim = thread_id;
        while (flag[1 - thread_id] && victim == thread_id) {}
    }
    
    public void unlock (int thread_id) {
        flag[thread_id] = false;
    }
}
