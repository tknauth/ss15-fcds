import java.util.concurrent.atomic.AtomicBoolean;

public class TASLock implements Lock {
    AtomicBoolean state = new AtomicBoolean(false);
    
    public void lock(int thread_id) {
        while (state.getAndSet(true)) {}
    }

    public void unlock(int thread_id) {
        state.set(false);
    }
}
