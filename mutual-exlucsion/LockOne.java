public class LockOne implements Lock {

    private volatile boolean[] flag = new boolean[2];

    // Open with Java debugger and see if it deadlocked.
    public void lock(int thread_id) {

        flag[thread_id] = true;
        int j = 1 - thread_id; // other thread's id
        while (flag[j]) {}
    }
    
    public void unlock (int thread_id) {

        flag[thread_id] = false;
    }
}
