#! /usr/bin/env bash

(for m in BackoffLock TASLock TTASLock AndersonLock CLHQueueLock MCSQueueLock ; do for threads in 1 2 4 8 16 24 ; do for i in $(seq 5) ; do print -n "$m $threads $i " ; \time -f'%e' java Main $m $threads > /dev/null ; done ; done ; done) 2>&1 | tee results.txt
