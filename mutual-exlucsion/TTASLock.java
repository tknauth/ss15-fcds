import java.util.concurrent.atomic.AtomicBoolean;

public class TTASLock implements Lock {
    AtomicBoolean state = new AtomicBoolean(false);
    
    public void lock(int thread_id) {
        while (true) {
            while (state.get()) {}
            if (!state.getAndSet(true)) return;
        }
    }

    public void unlock(int thread_id) {
        state.set(false);
    }
}
