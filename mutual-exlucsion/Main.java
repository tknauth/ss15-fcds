import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.math.BigInteger;


public class Main {

    private static final long LIMIT = 10000000L;
    private static int thread_count;
    
    private static class Compute implements Runnable {
        public static Lock theLock;
        public static long counter = 0;
        public static AtomicLong atomic = new AtomicLong(0);
        private int thread_id;
        
        public Compute(int thread_id) {
            this.thread_id = thread_id;
        }
        
        public void run() {
            while (true) {
                try {
                    theLock.lock(thread_id);
                    if (counter > LIMIT) return;
                    counter += 1;
                    // atomic.getAndIncrement();
                    // if (counter % 1000 == 0) System.out.println(""+counter);
                } finally {
                    theLock.unlock(thread_id);
                }
            }
        }
    }

    public static void parseArguments(String[] args) {

        thread_count = Integer.parseInt(args[1]);
        
        if (args[0].equals("LockOne")) {
            Compute.theLock = new LockOne();
        } else if (args[0].equals("LockTwo")) {
            Compute.theLock = new LockTwo();
        } else if (args[0].equals("PetersonLock")) {
            Compute.theLock = new PetersonLock();
        } else if (args[0].equals("FilterLock")) {
            Compute.theLock = new FilterLock(thread_count);
        } else if (args[0].equals("BakeryLock")) {
            Compute.theLock = new BakeryLock(thread_count);
        } else if (args[0].equals("TASLock")) {
            Compute.theLock = new TASLock();
        } else if (args[0].equals("TTASLock")) {
            Compute.theLock = new TTASLock();
        } else if (args[0].equals("BackoffLock")) {
            Compute.theLock = new BackoffLock();
        } else if (args[0].equals("AndersonLock")) {
            Compute.theLock = new AndersonLock(thread_count);
        } else if (args[0].equals("CLHQueueLock")) {
            Compute.theLock = new CLHQueueLock();
        } else if (args[0].equals("MCSQueueLock")) {
            Compute.theLock = new MCSQueueLock();
        } else {
            throw new RuntimeException();
        }
    }
    
    public static void main(String[] args) {

        parseArguments(args);
        
        // int threads = Integer.parseInt(args[0]);
        List<Thread> ts = new ArrayList<Thread>();
        for (int i = 0; i < thread_count; ++i) {
            Compute c = new Compute(i);
            Thread t = new Thread(c);
            t.start();
            ts.add(t);
        }

        for (Thread t : ts) {
            try {
                t.join();
                
            } catch (InterruptedException e) { throw new RuntimeException(); }
        }

        //Compute.theLock.check();
        // assert Compute.counter == Compute.atomic.get();
        
        System.out.println("atomic: "+Compute.atomic.get());
        System.out.println("found "+Compute.counter);
        // https://www.wolframalpha.com/input/?i=how+many+primes+are+there+below+10%5E9
    }
}
