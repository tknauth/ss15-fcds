#! /usr/bin/env python2.7

import argparse
import matplotlib
matplotlib.use('PDF')
import matplotlib.pyplot as plot
import sys
import numpy

if __name__ == '__main__' :

    ys_average = [ ]

    recs = []
    with open(sys.argv[1]) as f:
        for line in f:
            recs.append(line.strip().split())

    lock_types = set(map(lambda x : x[0], recs))
    thread_counts = set(map(lambda x : int(x[1]), recs))

    print lock_types
    print sorted(thread_counts)

    for lock_type in lock_types:
        ys = []
        err = []
        for thread_count in sorted(thread_counts):
            matches = filter(lambda r : (r[0] == lock_type and int(r[1]) == thread_count), recs)
            ys.append(numpy.average(map(lambda x : float(x[3]), matches)))
            err.append(numpy.std(map(lambda x : float(x[3]), matches)))
        plot.errorbar(sorted(thread_counts), ys, yerr=err, label=lock_type, lw=3)

plot.legend(loc='upper left')
plot.savefig('plot')
