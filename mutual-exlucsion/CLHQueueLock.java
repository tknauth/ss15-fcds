import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

class CLHQueueLock implements Lock {

    private class QNode {
        AtomicBoolean locked;
        public QNode(boolean state) {
             locked = new AtomicBoolean(state);
        }
    }
    AtomicReference<QNode> tail = new AtomicReference<QNode>(new QNode(false));
    ThreadLocal<QNode> myNode = new ThreadLocal<QNode>();
    
    public CLHQueueLock() {
    }
    
    public void lock(int thread_id) {
        QNode node = new QNode(true);
        QNode pred = tail.getAndSet(node);
        while (pred.locked.get()) {}
        myNode.set(node);
    }

    public void unlock(int thread_id) {
        QNode node = myNode.get();
        node.locked.set(false);
    }
}
