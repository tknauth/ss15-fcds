import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

class MCSQueueLock implements Lock {

    private class QNode {
        AtomicBoolean locked;
        AtomicReference<QNode> next;
        public QNode(boolean state) {
             locked = new AtomicBoolean(state);
             next = new AtomicReference<QNode>();
        }
    }
    AtomicReference<QNode> tail = new AtomicReference<QNode>();
    ThreadLocal<QNode> myNode = new ThreadLocal<QNode>();
    
    public void lock(int thread_id) {
        QNode node = new QNode(true);
        QNode pred = tail.getAndSet(node);
        if (pred != null) {
            pred.next.set(node);
            while (node.locked.get()) {}
        }
        myNode.set(node);
    }

    public void unlock(int thread_id) {
        QNode node = myNode.get();
        if (node.next.get() == null) {
            if (tail.compareAndSet(node, null)) {
                return;
            }
            while (node.next.get() == null) {}
        }
        QNode succ = node.next.get();
        succ.locked.set(false);
    }
}
