public class LockTwo implements Lock {

    private volatile int victim;

    public void lock(int thread_id) {
        victim = thread_id;
        while (victim == thread_id) {}
    }
    
    public void unlock (int thread_id) {
    }
}
