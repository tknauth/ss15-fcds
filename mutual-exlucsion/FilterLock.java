public class FilterLock implements Lock {

    private volatile int level[];
    private volatile int victim[];

    public FilterLock(int thread_count) {
        level = new int[thread_count];
        victim = new int[thread_count];
        for (int i = 0; i < thread_count; ++i) {
            level[i] = victim[i] = 0;
        }
    }
    
    public void lock(int thread_id) {
        for (int L = 1; L < level.length; ++L) {
            level[thread_id] = L;
            victim[L] = thread_id;
            while (is_same_or_higher_level(thread_id, L) &&
                   victim[L] == thread_id) {}
        }
    }

    private boolean is_same_or_higher_level(int thread_id, int level) {
        for (int i = 0; i < this.level.length; ++i) {
            if (i == thread_id) continue;
            if (this.level[i] >= level) return true;
        }
        return false;
    }
    
    public void unlock (int thread_id) {
        level[thread_id] = 0;
    }
}
