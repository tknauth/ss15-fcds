import java.util.concurrent.atomic.AtomicBoolean;
import java.util.Random;

public class BackoffLock implements Lock {
    AtomicBoolean state = new AtomicBoolean(false);

    private static int MIN_DELAY_MILLISECONDS = 5;
    private static int MAX_DELAY_MILLISECONDS = 100;
    private static Random prng = new Random();
    
    public void lock(int thread_id) {
        int delay = MIN_DELAY_MILLISECONDS;
        while (true) {
            while (state.get()) {}
            if (!state.getAndSet(true))
                return;
            try {
                Thread.sleep(prng.nextInt(delay));
            } catch (InterruptedException e) {}
            if (delay < MAX_DELAY_MILLISECONDS)
                delay = 2 * delay;
        }
    }

    public void unlock(int thread_id) {
        state.set(false);
    }
}
