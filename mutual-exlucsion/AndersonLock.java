import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

class AndersonLock implements Lock {
    private AtomicBoolean flags[];

    AtomicInteger next = new AtomicInteger(0);
    
    ThreadLocal<Integer> mySlot = new ThreadLocal<Integer>();

    public AndersonLock(int threads) {
        flags = new AtomicBoolean[threads];
        flags[0] = new AtomicBoolean(false);
        for (int i = 1; i < threads; ++i) {
            flags[i] = new AtomicBoolean(true);
        }
    }
    
    public void lock(int thread_id) {
        int slot = next.getAndIncrement();
        while (flags[slot % flags.length].get()) {}
        flags[slot % flags.length].set(true);
        mySlot.set(new Integer(slot));
    }

    public void unlock(int thread_id) {
        int slot = mySlot.get().intValue();
        flags[(slot + 1) % flags.length].set(false);
    }
}
